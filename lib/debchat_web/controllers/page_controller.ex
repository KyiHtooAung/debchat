defmodule DebchatWeb.PageController do
  use DebchatWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
